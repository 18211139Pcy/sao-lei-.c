#ifndef __GAME_H__
#define __GAME__H__

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
struct a{
	int row;
	int col;
	char show_mine[102][102];
	char real_mine[102][102];
}s;
struct k{
	int count1;
	int count2;
	int count3;
	int count4;
	int count;
	
}p;
void init_mine()
{
    int i = 0;
    int j = 0;
    for (i = 1; i < s.row+1; i++) 
    {
        for (j = 1; j < s.col+1; j++)
        {

		    s.show_mine[i][j] = '*';
            s.real_mine[i][j] = '0';
		}
        
    }
}


void print_player()
{
    int i = 0;
    int j = 0;
    printf("0  ");
    for (i = 1; i <s.col+1; i++)
    {
       if(i<10)
	    printf("%d  ", i);
	    else
	    printf("%d ", i);
	    
    }
    printf("\n");
    for (i = 1; i <s.row; i++)
    {
        if(i<10)
        printf("%d  ", i);
        else
        printf("%d ", i);
        for (j = 1; j < s.col+1; j++)
        {

            printf("%c  ", s.show_mine[i][j]);
        }
        printf("\n");
    }
    
       printf("%d ",s.row);
       if(s.row<10) 
       printf(" ");
    for (i = 1; i < s.col+1; i++)
    {

        printf("%c  ", s.show_mine[s.row][i]);
    }
    printf("\n");
}


void print_mine()
{
    int i = 0;
    int j = 0;
    printf("0  ");
    for (i = 1; i <s.col+1; i++)
    {
    	if(i<10)
		  printf("%d  ", i);
		  else
		  printf("%d ", i);
	}
    printf("\n");
    for (i = 1; i <s.row ; i++)
    {   
        if(i<10)
        printf("%d  ", i);
        else
        printf("%d ", i);
        for (j = 1; j < s.col + 1; j++)
        {
              
			   printf("%c  ", s.real_mine[i][j]);
        }
        printf("\n");
    }
    printf("%d ",s.row);
    if(s.row<10)
    printf(" ");
    for (i = 1; i < s.col + 1; i++)
    { 

           printf("%c  ", s.real_mine[s.row][i]);       
    }
    printf("\n");
}


void set_mine()
{
    int x = 0;
    int y = 0;


    while (p.count1)
    {
        int x = rand() % 5+1;
        
        int y = rand() % 5+1;
        if (s.real_mine[x][y] == '0'&&x!=1&&x!=5&&y!=1&&y!=5)
        {
            s.real_mine[x][y] = '1';
            p.count1--;
        }
        if(x==3&&y==3){
            s.real_mine[x][y] = '0';
            p.count1++; 
		}
	}
    while (p.count2)
    {
        int x = rand() % 5+6;
        
        int y = rand() % 5+1;
        if (s.real_mine[x][y] == '0'&&x!=6&&x!=10&&y!=1&&y!=5)
        {
            s.real_mine[x][y] = '1';
            p.count2--;
        }
        if(x==8&&y==3){
            s.real_mine[x][y] = '0';
            p.count2++; 
		}
	}
	 while (p.count3)
    {
        int x = rand() % 5+1;
        
        int y = rand() % 5+6;
        if (s.real_mine[x][y] == '0'&&x!=1&&x!=5&&y!=6&&y!=10)
        {
            s.real_mine[x][y] = '1';
            p.count3--;
        }
        if(x==3&&y==8){
            s.real_mine[x][y] = '0';
            p.count3++; 
		}
	}
    while (p.count4)
    {
        int x = rand() % 5+6;
        
        int y = rand() % 5+6;
        if (s.real_mine[x][y] == '0'&&x!=6&&x!=10&&y!=6&&y!=10)
        {
            s.real_mine[x][y] = '1';
            p.count4--;
        }
        if(x==8&&y==8){
            s.real_mine[x][y] = '0';
            p.count4++; 
		}
	}
}
int count_mine(int x, int y)
{
    int n= 0;
    if (s.real_mine[x - 1][y - 1] == '1')
        n++;
    if (s.real_mine[x - 1][y] == '1')
        n++;    
    if (s.real_mine[x - 1][y + 1] == '1')
        n++;
    if (s.real_mine[x][y - 1] == '1')
        n++;    
    if (s.real_mine[x][y + 1] == '1')
        n++;    
    if (s.real_mine[x + 1][y - 1] == '1')
        n++;    
    if (s.real_mine[x + 1][y] == '1')
        n++;    
    if (s.real_mine[x + 1][y + 1] == '1')
        n++;
    return n;
}


void open_mine(int x, int y)
{
    if( s.show_mine[x][y]=='0'){

    if (s.real_mine[x - 1][y - 1]== '0'&&s.show_mine[x - 1][y - 1]=='*')
    {
        s.show_mine[x - 1][y - 1] = count_mine(x - 1, y - 1) + '0';
        if(s.show_mine [x - 1][y - 1]=='0')
           open_mine(x-1,y-1);
    }
    if (s.real_mine[x - 1][y] == '0'&&s.show_mine[x - 1][y]=='*')
    {
        s.show_mine[x - 1][y] = count_mine(x - 1, y) + '0';
        if(s.show_mine [x - 1][y]=='0')
           open_mine(x-1,y);
    }
    if (s.real_mine[x - 1][y + 1] == '0'&&s.show_mine[x - 1][y + 1]=='*')
    {
        s.show_mine[x - 1][y + 1] = count_mine(x - 1, y + 1) + '0';
                if(s.show_mine [x - 1][y + 1]=='0')
           open_mine(x-1,y+1);
    }
    if (s.real_mine[x][y - 1] == '0'&&s.show_mine[x][y - 1]=='*')
    {
        s.show_mine[x][y - 1] = count_mine(x, y - 1) + '0';
                if(s.show_mine [x][y - 1]=='0')
           open_mine(x,y-1);
    }
    if (s.real_mine[x][y + 1] == '0'&&s.show_mine[x][y + 1]=='*')
    {
        s.show_mine[x][y + 1] = count_mine(x, y + 1) + '0';
                if(s.show_mine [x][y + 1]=='0')
           open_mine(x,y+1);
    }
    if (s.real_mine[x + 1][y - 1] == '0'&&s.show_mine[x + 1][y - 1]=='*')
    {
        s.show_mine[x + 1][y - 1] = count_mine(x + 1, y - 1) + '0';
                if(s.show_mine [x + 1][y - 1]=='0')
           open_mine(x+1,y-1);
    }
    if (s.real_mine[x + 1][y] == '0'&&s.show_mine[x + 1][y]=='*')
    {
        s.show_mine[x + 1][y] = count_mine(x + 1, y) + '0';
                if(s.show_mine [x + 1][y]=='0')
           open_mine(x+1,y);
    }
    if (s.real_mine[x + 1][y + 1] == '0'&&s.show_mine[x + 1][y + 1]=='*')
    {
        s.show_mine[x + 1][y + 1] = count_mine(x + 1, y + 1) + '0';
                if(s.show_mine [x + 1][y + 1]=='0')
           open_mine(x+1,y+1);
    }
}
	else if((x==3&&y==3)||(x==3&&y==8)||(x==8&&y==3)||(x==8&&y==8)){
		s.show_mine[x][y]=count_mine(x,y)+'0';
	
	}
}

void safe_mine()
{
    int a,b;
	int x = 0;
    int y = 0;
    char ch = 0;
    int ret = 1;
    printf("输入坐标扫雷\n");
    while (1)
    {
        scanf("%d%d", &x, &y);
		a=x;
		b=y;
        if ((x >= 1 && x <= s.row) && (y >= 1 && y <= s.col))
        {
            if (s.real_mine[x][y] == '1')
            {
                s.real_mine[x][y] = '0';
                ch = count_mine(x, y);
                s.show_mine[x][y] = ch + '0';
                open_mine(x, y);
                while (ret)
                {
					if(x<=5&&y<=5){
					do{
                    x = rand() % 5+1;
                    y = rand() % 5+1;
						}	while(x==3&&y==3&&x==a&&y==b);
                    if (s.real_mine[x][y] == '0'&&x!=1&&x!=5&&y!=1&&y!=5)
                    {
                        s.real_mine[x][y] = '1';
                        ret--;
                        break;
                    }
					}
					if(x<=10&&x>=6&&y<=5){
						do{
                    x = rand() % 5+6;
                    y = rand() % 5+1;
						}while(x==8&&y==3&&x==a&&y==b);
                    if (s.real_mine[x][y] == '0'&&x!=10&&x!=6&&y!=1&&y!=5)
                    {
                        s.real_mine[x][y] = '1';
                        ret--;
                        break;
                    }
					}					
					if(x<=5&&y>=6&&y<=10){
						do{
                    x = rand() % 5+1;
                    y = rand() % 5+6;
						}while(x==3&&y==8&&x==a&&y==b);
                    if (s.real_mine[x][y] == '0'&&x!=1&&x!=5&&y!=10&&y!=6)
                    {
                        s.real_mine[x][y] = '1';
                        ret--;
                        break;
                    }
					}
					if(x<=10&&x>=6&&y<=10&&y>=6){
						do{
                    x = rand() % 5+6;
                    y = rand() % 5+6;
						}while(x==8&&y==8&&x==a&&y==b);
                    if (s.real_mine[x][y] == '0'&&(x!=10&&x!=6&&y!=6&&y!=10))
                    {
                        s.real_mine[x][y] = '1';
                        ret--;
                        break;
                    }
					}
                }break;  
            }
            if (s.real_mine[x][y] == '0')
            {
                char ch = count_mine(x, y);
                s.show_mine[x][y] = ch + '0';
                open_mine(x, y);
                break;
            }
        }
        else
        {
            printf("输入错误重新输入\n");
        }
    }
}

int count_show_mine()
{
    int n= 0;
    int i = 0;
    int j = 0;
    for (i = 1; i <= s.row; i++)
    {
        for (j = 1; j <= s.col; j++)
        {
            if (s.show_mine[i][j] == '*')
            {
                n++;
            }
        }

    } 
    return n;
}

int sweep_mine()
{
    int x = 0;
    int y = 0;
    char z = 0;
    printf("输入坐标扫雷,尾标为1则标记雷，不加尾标则扫雷\n");
        scanf("%d%d", &x, &y);
        if('\n'==getchar()){
        	if ((x >= 1 && x <= s.row) && (y >= 1 && y <= s.col))
        {
        	
            if (s.real_mine[x][y] == '0')
            {
                char ch = count_mine(x,y);
                s.show_mine[x][y] = ch+'0';
                open_mine(x, y);
                if (count_show_mine() == p.count)
                {  
                    print_mine();
                    printf("玩家赢！\n\n");
                    return 0;
                }
            }
            else if (s.real_mine[x][y]=='1')
                return 1;
            }

        else
        {
            printf("输入错误重新输入\n");
        }
		}
		else{
		
        z=getchar();
        if(z!='1')
		{
        if ((x >= 1 && x <= s.row) && (y >= 1 && y <= s.col))
        {
        	
            if (s.real_mine[x][y] == '0')
            {
                char ch = count_mine(x,y);
                s.show_mine[x][y] = ch+'0';
                open_mine(x, y);
                if (count_show_mine() == p.count)
                {
                    print_mine();
                    printf("玩家赢！\n\n");
                    return 0;
                }
            }
            else if (s.real_mine[x][y]=='1')
                return 1;
            }

        else
        {
            printf("输入错误重新输入\n");
        }
		}
		
		else{
			s.show_mine[x][y]='#';
		}
	}
    return 0;
}

void muen()
{
    printf("*******************************\n");
    printf("*****1.play       0.exit*******\n");
    printf("*******************************\n");
}

void game()
{

    int ret = 0;
    init_mine();
    set_mine();
    open_mine(3,3);
    open_mine(8,3);
	open_mine(3,8);
	open_mine(8,8);
    print_mine();
    printf("\n");
    print_player();
    safe_mine();

    if (count_show_mine() == p.count)
    {
        print_mine();
        printf("玩家赢！\n\n");
        return ;
    }print_player();

    while (1)
    {
        int ret=sweep_mine();
        if (count_show_mine() ==p.count)
        {  
            print_mine();
            printf("玩家赢！\n\n");
            break;
        }
        if (ret)
        {
            printf("被雷炸死\t");
            printf("you are a 菜鸟\n");
            print_mine();
            break;
        }print_player();
    }
}

int main()
{
    int input,i,j;
    printf("请选择游戏难度:简单（1）  中级（2）   困难（3）  \n");
   		s.row=s.col=10;
   		scanf("%d",&i);
	if(i==1){
	    p.count1 = rand() % 2+6;
	    p.count2 = rand() % 2+6;
	    p.count3 = rand() % 2+6;
	    p.count4 = rand() % 2+6;
	}
	else if(i==2){
	    p.count1 = rand() % 2+5;
	    p.count2 = rand() % 2+5;
	    p.count3 = rand() % 2+5;
	    p.count4 = rand() % 2+5;
	}
	if(i==3){
	    p.count1 =4;
	    p.count2 =4;
	    p.count3 =4;
	    p.count4 =4;
	}
	p.count=p.count1+p.count2+p.count3+p.count4;

    for(i=0;i<=s.col+1;i++){
    	for(j=0;j<s.row+2;j++){
		   
    	 s.show_mine[j][i]=' ';
         s.real_mine[j][i]=' ';
		 }
	}
	input=0;
    srand((unsigned int)time(NULL));
    muen();
    do
    {
        scanf("%d", &input);
        switch (input)
        {
        case 1:game();
            break;
        case 0:exit(1);
            break;
        default:
            printf("输入错误，重新输入\n");
            break;
        }
        muen();
        printf("contiue?\n");
    } while (1);
    system("pause");
    return 0;
#endif;
}